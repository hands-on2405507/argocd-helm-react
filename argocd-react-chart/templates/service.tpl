apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.service.name }}-service
spec:
  selector:
    app: {{ .Values.deploymentName }}
  type: NodePort
  ports:
    - port: {{ .Values.service.port }} #Port on which Service will get request
      targetPort: {{ .Values.container.port }} #Port on which Container inside Pod is running and Port on which Service will forward request to
      nodePort: 31000