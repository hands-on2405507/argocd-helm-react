apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.deploymentName }}-deployment
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ .Values.deploymentName }}
  template:
    metadata:
      labels:
        app: {{ .Values.deploymentName }}
    spec:
      containers:
        - name: {{ .Values.container.name }}-container
          image: {{ .Values.container.imageName }}
          imagePullPolicy: {{ .Values.container.imagePullPolicy | default "Always" }} 
          ports:
            - containerPort: {{ .Values.container.port | default 80 }}
    
